package combopt;

public class Edge {
    private final int firstVertexId;
    private final int secondVertexId;
    private final int weight;

    public Edge(int firstVertexId, int secondVertexId, int weight) {
        this.firstVertexId = firstVertexId;
        this.secondVertexId = secondVertexId;
        this.weight = weight;
    }

    public int getFirstVertexId() {
        return firstVertexId;
    }

    public int getSecondVertexId() {
        return secondVertexId;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return firstVertexId + " " + secondVertexId;
    }

    @Override
    public int hashCode() {
        int result = 17;
        int tempLower = Integer.min(firstVertexId,secondVertexId);
        int tempBigger = Integer.max(firstVertexId,secondVertexId);

        result = 31 * result + tempLower;
        result = 31 * result + tempBigger;
        result = 31 * result + weight;
        return result;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Edge)) {
            return false;
        }

        Edge edge = (Edge) o;

        return (edge.getFirstVertexId() == firstVertexId || edge.getFirstVertexId() == secondVertexId)&&
                (edge.getSecondVertexId() == secondVertexId || edge.getSecondVertexId() == firstVertexId)&&
                edge.getWeight() == (weight);
    }
}
