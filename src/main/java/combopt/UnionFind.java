package combopt;

public class UnionFind {
    private Vertex[] vertices;
    private int weight;

    public UnionFind(int numberOfVertices,boolean initArray) {
        this.vertices = new Vertex[numberOfVertices];
        this.weight = 0;
        if(initArray) {
            initArray(numberOfVertices);
        }
    }

    private void initArray(int numberOfVertices) {
        for (int i = 0; i < numberOfVertices ; i++) {
            vertices[i] = new Vertex(i);
        }
    }

    public int findLargestComponent() {
        int largestSize = 0;
        int centralVertex = -1;

        for (Vertex vertex : vertices) {
            if(vertex.getComponentSize() > largestSize) {
                largestSize = vertex.getComponentSize();
                centralVertex = vertex.getValue();
            }
        }
        return centralVertex;
    }

    public Vertex find(int vertexValue) {
        Vertex vertex = vertices[vertexValue];
        while(!vertex.isRoot()) {
            vertex = vertices[vertex.getParent()];
        }
        return vertex;
    }

    public boolean union(Edge edge) {
        Vertex root1 = find(edge.getFirstVertexId());
        Vertex root2 = find(edge.getSecondVertexId());

        if(root1 != root2) {
            weight+= edge.getWeight();
            if(root1.getComponentSize() < root2.getComponentSize()) {
                root1.setParent(root2);
            } else {
                root2.setParent(root1);
            }
            return true;
        }
        return false;
    }

    public int getWeight() {
        return weight;
    }

}
