package combopt;

public class GraphVertex {
    private final int value;
    private boolean isTerminal = false;

    public GraphVertex(int value) {
        this.value = value;
    }

    public void setTerminal() {
        this.isTerminal = true;
    }

    public int getValue() {
        return value;
    }

    public boolean isTerminal() {
        return isTerminal;
    }
}
