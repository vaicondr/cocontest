package combopt;


import java.util.*;

public class SimpleMapGraph {
    private Map<Integer,Integer>[] weightedGraphMap;
    private GraphVertex[] vertices;

    private int price;
    private int numberOfVertices;


    public SimpleMapGraph(int numberOfVertices) {
        this.weightedGraphMap = new HashMap[numberOfVertices+1];
        this.price = 0;
        this.numberOfVertices = numberOfVertices;
        this.vertices = new GraphVertex[numberOfVertices];
        for (int i = 0; i < numberOfVertices; i++) {
            vertices[i] = new GraphVertex(i);
        }
    }

    public void addEdge(Edge edge) {
        Map<Integer,Integer> map1 = weightedGraphMap[edge.getFirstVertexId()];

        if(map1 == null) {
            map1 = new HashMap<>();
        }

        map1.put(edge.getSecondVertexId(),edge.getWeight());
        weightedGraphMap[edge.getFirstVertexId()] = map1;

        Map<Integer,Integer> map2 = weightedGraphMap[edge.getSecondVertexId()];

        if(map2 == null) {
            map2 = new HashMap<>();
        }

        map2.put(edge.getFirstVertexId(),edge.getWeight());
        weightedGraphMap[edge.getSecondVertexId()] = map2;


        price += edge.getWeight();
    }

    public void removeEdges(List<Edge> edges) {
        for(Edge edge : edges) {
            removeEdge(edge);
        }
    }

    public void removeEdge(Edge edge) {
        Map<Integer,Integer> map1 = weightedGraphMap[edge.getFirstVertexId()];
        map1.remove(edge.getSecondVertexId());
        weightedGraphMap[edge.getFirstVertexId()] = map1;

        Map<Integer,Integer> map2 = weightedGraphMap[edge.getSecondVertexId()];
        map2.remove(edge.getFirstVertexId());
        weightedGraphMap[edge.getSecondVertexId()] = map2;

        price -= edge.getWeight();
    }

    public List<Edge> getNeighbours(int vertex) {
        List<Edge> neighbours = new ArrayList<>();
        Map<Integer,Integer> map = weightedGraphMap[vertex];
        for(Map.Entry<Integer,Integer> entry : map.entrySet()) {
            neighbours.add(new Edge(vertex,entry.getKey(),entry.getValue()));
        }
        return neighbours;
    }

    public int getEdgePrice(int vertex1, int vertex2) {
        return weightedGraphMap[vertex1].get(vertex2);
    }


    public List<Edge> getAllEdges() {
        Set<Edge> edges = new HashSet<>();
        for (int i = 1; i <= numberOfVertices ; i++) {
            if(weightedGraphMap[i] != null) {
                Map<Integer,Integer> map = weightedGraphMap[i];
                for(Map.Entry<Integer,Integer> mapEntry : map.entrySet()) {
                    edges.add(new Edge(i,mapEntry.getKey(),mapEntry.getValue()));
                }
            }
        }
        return new ArrayList<>(edges);
    }

    public void setTerminals(List<Integer> terminals) {
        for(Integer terminal : terminals) {
            vertices[terminal].setTerminal();
        }
    }

    public boolean isTerminal(int value) {
        return vertices[value].isTerminal();
    }


    @Override
    public Object clone()  {
        SimpleMapGraph clone = new SimpleMapGraph(numberOfVertices);
        for (int i = 0; i <= numberOfVertices; i++) {
            if (weightedGraphMap[i] != null) {
                clone.weightedGraphMap[i] = new HashMap<>(weightedGraphMap[i]);
            }
        }
        clone.price = price;
        return clone;
    }

    public int getNumberOfVertices() {
        return numberOfVertices;
    }


    public int getPrice() {
        return price;
    }

    private String allEdgesToString() {
        StringBuilder output = new StringBuilder();
        List<Edge> edges = getAllEdges();
        for(Edge edge : edges) {
            output.append(edge.toString())
                    .append("\n");
        }
        return output.toString();
    }

    @Override
    public String toString() {
        return getPrice() + "\n" + allEdgesToString() ;
    }
}
