package combopt;


import java.util.*;

public class Solver {
    int minWeight = 0;
    int centralVertex = -1;

    public String solveOptimalConnection(List<Edge> edges, List<Integer> nobleTowns, int numberOfVertices) {
//        edges.sort(Comparator.comparingInt(Edge::getWeight));
        UnionFind mst = new UnionFind(numberOfVertices,true);
        SimpleMapGraph mstGraph = findMST(edges, mst, numberOfVertices);
        mstGraph.setTerminals(nobleTowns);
        cutOffNonNobleTerminals(mstGraph);
        return mstGraph.toString();
    }



    private void cutOffNonNobleTerminals(SimpleMapGraph graph) {
        Deque<Integer> openSet = new ArrayDeque<>();
        Set<Integer> closedSet = new HashSet<>();
        openSet.add(centralVertex);

        while(!openSet.isEmpty()) {
            int vertex = openSet.poll();
            boolean allChildClosed = true;
            List<Edge> neighbours = graph.getNeighbours(vertex);
            for(Edge edge: neighbours) {
                int child = edge.getSecondVertexId();
                if(closedSet.contains(child)) {
                    continue;
                }
                allChildClosed = false;
                if(!openSet.contains(child)) {
                    openSet.addLast(child);
                }
            }
            if(allChildClosed && !graph.isTerminal(vertex) && neighbours.size() == 1) {
                for(Edge edge: neighbours) {
                    closedSet.remove(edge.getSecondVertexId());
                    openSet.addFirst(edge.getSecondVertexId());
                }
                graph.removeEdges(neighbours);
            } else {
                closedSet.add(vertex);
            }
        }
    }

    private SimpleMapGraph findMST(List<Edge> orderedEdges,UnionFind mst,int numberOfVertices) {
        SimpleMapGraph mstGraph = new SimpleMapGraph(numberOfVertices);
        for(Edge edge : orderedEdges) {
            if(mst.union(edge)) {
                mstGraph.addEdge(edge);
            }
        }
        centralVertex = mst.findLargestComponent();
        return mstGraph;
    }

}
