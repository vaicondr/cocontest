package combopt;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String... args) {
        int numberOfVertices, numberOfEdges;
        List<Integer> nobleTowns = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();

        String inputFile = args[0];
        String outputFile = args[1];


        try(BufferedReader   bi = new BufferedReader(new FileReader(inputFile))) {
            String line = bi.readLine();
            String[] input = line.split(" ");
            numberOfVertices = Integer.parseInt(input[0]);
            numberOfEdges = Integer.parseInt(input[1]);
            for (int i = 0; i < numberOfEdges; i++) {
                line = bi.readLine();
                input = line.split(" ");
                edges.add(new Edge(Integer.parseInt(input[0]), Integer.parseInt(input[1]), Integer.parseInt(input[2])));
            }
            line = bi.readLine();
            input = line.split(" ");
            for( String number : input) {
                nobleTowns.add(Integer.parseInt(number));
            }
            Solver solver = new Solver();
            String output = solver.solveOptimalConnection(edges,nobleTowns,numberOfVertices);
            writeIntoFile(outputFile, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeIntoFile(String outputFile,String contents) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))) {
            bw.write(contents);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
