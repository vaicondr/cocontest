package combopt;

public class Vertex {
    private int parentValue;
    private int value;
    private int componentSize;

    public Vertex(int value) {
        this.parentValue = value;
        this.value = value;
        this.componentSize = 1;
    }

    public Vertex(Vertex originalVertex) {
        this.parentValue = originalVertex.parentValue;
        this.value = originalVertex.value;
        this.componentSize = originalVertex.componentSize;
    }

    public boolean isRoot() {
        return this.value == parentValue;
    }

    public int getParent() {
        return parentValue;
    }

    public int getValue() {
        return value;
    }

    public int getComponentSize() {
        return componentSize;
    }

    public void setParent(Vertex parent) {
        this.parentValue = parent.value;
        parent.componentSize += this.componentSize;
    }
}
